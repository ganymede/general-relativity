# General Relativity Theory

This project is part of for the german [lecture on general relativity theory](https://itp.uni-frankfurt.de/~hanauske/VARTC/) by [Dr. Matthias Hanauske](https://itp.uni-frankfurt.de/~hanauske/)

The program [geodaetic_solution.py](geodaetic_solution.py) can solve the problem of a single body moving around a normal (non rotating) black hole.


## Usage

### Online
An online version can be found on [Google Colab](https://colab.research.google.com/drive/19BvTnX9fhGz-4bMiZbrcp_2FJB-Ncxak?usp=sharing).

### Local
The [Jupyter Notebook](geodaetic_solution.ipynb) can be run inside jupyterlab.
A bespoke [docker image](https://gitlab.com/ganymede/jupyterlab) can be used to run it locally:
```
docker run --rm -it -p 8888:8888 -e PASSWORD="<your_secret>" -e GIT_URL="https://gitlab.com/ganymede/general-relativity.git" registry.gitlab.com/ganymede/jupyterlab:latest
```

## Example
If a body is starting in the following coordinates (as polar coordinates):

| Coordinate| Value |
|:----------|------:|
| $` r_0 `$ | 9 km |
| $` \phi_0 `$ | 0 |
| $` \theta_0 `$ | $`\pi / 2`$ |

with the following initial velocities:

| Velocity  | Value |
|:----------|------:|
| $` \frac{d}{d\tau} r_0 `$ | 0 |
| $` \frac{d}{d\tau} \phi_0 `$ | 0.055185 |
| $` \frac{d}{d\tau} \theta_0 `$ | 0 |

Additionally we are focusing only on planar movement and restricting $` \frac{d^2}{d\tau^2} \theta = 0`$.

The code calculates the following trajectory:  
![geodaetic_solution.mp4](https://gitlab.com/ganymede/general-relativity/-/jobs/645314694/artifacts/raw/geodaetic_solution.mp4)

![geodaetic_solution.png](https://gitlab.com/ganymede/general-relativity/-/jobs/645314694/artifacts/raw/geodaetic_solution.png)

## Summary of tensor calculation
The [Basics of tensors](Basics-of-tensors-German.pdf) are a handwritten short summary of the recorded lecture on [Einführung in die Vektor- und Tensorrechnung II](https://www.youtube.com/playlist?list=PLrWrjvhC1doZ-1deZ9eM7NYoewh2tM2i3) by Prof. Paul Wagner.